﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlayerMovement : MonoBehaviour
{
    public float speed = 4;
    public float rotationSpeed = 180;
    
    [HideInInspector]
    public float horizontalMove;
    [HideInInspector]
    public float verticalMove;


    public Rigidbody rb;

    [HideInInspector]
    public Vector3 moveDirection;

    public bool isRunning;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    // Update is called once per frame
    void Update()
    {
        horizontalMove = Input.GetAxis("Horizontal");
        verticalMove = Input.GetAxis("Vertical");

        Move();

    }

    #region Movement

    void Move()
    {
        

        moveDirection = new Vector3(horizontalMove, 0, verticalMove);
        moveDirection.Normalize();

        transform.Translate(moveDirection * speed * Time.deltaTime, Space.World);       
    }
    #endregion
}