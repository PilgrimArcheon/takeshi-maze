﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stage : MonoBehaviour
{
    public int stageID;
    public bool open;
    public Button button;
    public Image stageImage;
    public Text text;

    public Color[] color;
    

    int curStage;

    // Start is called before the first frame update
    void Start()
    {
        curStage = SaveManager.Instance.state.stage;

        if(stageID <= curStage )
        {
            button.interactable = true;
            stageImage.color = color[0];
            text.color = color[0];
        }
        else
        {
            button.interactable = false;
            stageImage.color = color[2];
            text.color = color[2];
        }
    }


    public void Select()
	{
		open = true;
        stageImage.color = color[1];
        text.color = color[1];
		
	}

	public void Deselect()
	{
		open = false;
        stageImage.color = color[0];
        text.color = color[0];
	}
}
