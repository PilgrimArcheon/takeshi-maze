﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexGrid : MonoBehaviour
{
    public Transform hexPrefab;

    public int numberOfRooms;
    public int numberOfMonsters;

    public int gridWidth = 10;
    public int gridHeight = 10;


    float hexWidth = 17.32f;
    float hexHeight = 20.0f;

    public float gap = 0.0f;

    Vector3 startPos;

    List<int> numbers = new List<int>();
    public List<Transform> rooms = new List<Transform>();

    void Start()
    {
        Invoke("Play", 0.3f);
        numberOfRooms = LevelManager.Instance.noR;
        numberOfMonsters = LevelManager.Instance.noM;
    }

    void Play() 
    {
        Num();
        AddGap();
        CalcStartPos();
        StartCoroutine(CreateGrid());
    }

    void Num()
    {
        int number = numberOfRooms;

        if(numberOfRooms <= 50)
        {
            for (int divisor = 2; divisor < 10; divisor++)
            {
                if((number % divisor) == 0)
                {
                    numbers.Add(divisor);
                }
            }
        }

        else if(numberOfRooms > 50  && numberOfRooms <= 100)
        {
            for (int divisor = 2; divisor <= 10; divisor++)
            {
                if((number % divisor) == 0)
                {
                    numbers.Add(divisor);
                }
            }
        }

        else
        {
            for (int divisor = 2; divisor <= 20; divisor++)
            {
                if((number % divisor) == 0)
                {
                    numbers.Add(divisor);
                }
            }
        }
        

        gridWidth = numbers[numbers.Count -1];
        gridHeight = numberOfRooms /gridWidth;
    }

    void AddGap()
    {
        hexWidth += hexWidth * gap;
        hexHeight += hexHeight * gap;
    }

    void CalcStartPos()
    {
        float offset = 0;
        if(gridHeight / 2 % 2 != 0)
            offset = hexWidth / 2;

        float x = -hexWidth * (gridWidth / 2) - offset;
        float z = hexHeight * 0.75f * (gridHeight / 2);

        startPos = new Vector3(x, 0, z);
    }

    Vector3 CalcWorldPos(Vector2 gridPos)
    {
        float offset = 0;
        if (gridPos.y % 2 !=0)
            offset = hexWidth / 2;

        float x = startPos.x + gridPos.x * hexWidth + offset;
        float z = startPos.z - gridPos.y * hexHeight * 0.75f;

        return new Vector3(x, 0, z);
    }

    IEnumerator CreateGrid()
    {
        for (int y = 0; y < gridHeight; y++)
        {
            for (int x = 0; x < gridWidth; x++)
            {
                Transform hex = Instantiate(hexPrefab) as Transform;
                Vector2 gridPos = new Vector2(x, y);
                hex.position = CalcWorldPos(gridPos);
                hex.parent = this.transform;
                hex.name = "Hexagon" + x + "|" + y;
                rooms.Add(hex);
                
                yield return new WaitForSeconds(0.2f);
            }
        }
    }  
 
}
