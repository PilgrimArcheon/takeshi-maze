﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallDest : MonoBehaviour
{
    void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.tag == "Wall")
        {
            Destroy(other.gameObject, 0.1f); 
        }
    }
}
