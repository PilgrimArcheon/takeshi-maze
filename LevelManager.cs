﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
	public static LevelManager Instance;

    [SerializeField] Level[] levels;
    [SerializeField] Stage[] stages;

    public int[] NumOfRooms;
    public int[] NumOfMonsters;
    public float[] timeForLevel;



    public int curLevel; 
    public int curStage;
    public int noR;
    public int noM;
    public float tfL;

	void Awake()
	{
		Instance = this;
        DontDestroyOnLoad(gameObject);
	}

    public void PlayLevel()
    {
        for(int i = 0; i < levels.Length; i++)
		{   
			if(levels[i].open)
			{
                noR = NumOfRooms[i];
                tfL = timeForLevel[i];

                for (int j = 0; j < stages.Length; j++)
                {
                    if(stages[j].open)
                    {
                        noM = NumOfMonsters[j];
                    }
                }
			}
		} 

    }

	public void SelectLevel(int levelID)
	{
		for(int i = 0; i < levels.Length; i++)
		{
			if(levels[i].levelID == levelID)
			{
				levels[i].Select();
			}
			else if(levels[i].open)
			{
				DeselectLevel(levels[i]);
			}
		}
	}

	public void SelectLevel(Level level)
	{
		for(int i = 0; i < levels.Length; i++)
		{
			if(levels[i].open)
			{
				DeselectLevel(levels[i]);
			}
		}
		level.Select();
	}

	public void DeselectLevel(Level level)
	{
		level.Deselect();
	}

    ////////STAGE SELECT////////////

    public void SelectStage(int stageID)
	{
		for(int i = 0; i < stages.Length; i++)
		{
			if(stages[i].stageID == stageID)
			{
				stages[i].Select();
			}
			else if(stages[i].open)
			{
				DeselectStage(stages[i]);
			}
		}
	}

	public void SelectStage(Stage stage)
	{
		for(int i = 0; i < stages.Length; i++)
		{
			if(stages[i].open)
			{
				DeselectStage(stages[i]);
			}
		}
		stage.Select();
	}

	public void DeselectStage(Stage stage)
	{
		stage.Deselect();
	}
}
