﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexRoomManager : MonoBehaviour
{
    GameObject startRoom;
    GameObject startDoorPos;
    GameObject startDoorPos2;

    Transform endRoom;
    Transform spawnPoint;
    public Transform grid;

    public GameObject monster;

    public HexGrid hexGrid;

    public GameObject startDoor;
    public GameObject Door;
    public GameObject escapeDoor;
    
    public GameObject player;

    int roomNum;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("SetStartRoom", 1f);
        Invoke("PlaceMonsters", 7f);
        hexGrid = GetComponent<HexGrid>();
    }

    void SetStartRoom()
    {
        startRoom = GameObject.Find("Hexagon0|0");
        startDoorPos = startRoom.transform.GetChild(3).gameObject;
        startDoorPos2 = startRoom.transform.GetChild(0).gameObject;

        player.transform.position = startRoom.transform.position;
        player.transform.rotation = startRoom.transform.rotation;
    
        Invoke("StartDoor", 0.4f);
    }

    void StartDoor()
    {
        Instantiate(startDoor, startDoorPos.transform.position, startDoorPos.transform.rotation);
        Instantiate(startDoor, startDoorPos2.transform.position, startDoorPos2.transform.rotation);
    }

    void PlaceMonsters()
    {
        for (int i = 0; i <= LevelManager.Instance.noM - 1; i++)
        {
            int rand;
            //make room arrangement better
            if(LevelManager.Instance.noR > 20)
            {
                rand = Random.Range(3, LevelManager.Instance.noR - 10);
            }
            else
            {
                rand = Random.Range(3, LevelManager.Instance.noR - 3);
            }
            
            spawnPoint = grid.GetChild(rand).gameObject.transform;
            Instantiate(monster, spawnPoint.position, spawnPoint.rotation);

            Debug.Log(rand);
        }

        StartCoroutine(EndRoom());
    }
    

    IEnumerator EndRoom()
    {
        
        for (int i = LevelManager.Instance.noR - 3; i <= LevelManager.Instance.noR -1; i++)
        {
            endRoom = grid.GetChild(i).gameObject.transform;

            for (int j = 0; j <= 2; j++)
            {
                spawnPoint = endRoom.GetChild(j).gameObject.transform;
            }

            if(i == LevelManager.Instance.noR -2)
            {
                spawnPoint = endRoom.GetChild(0).gameObject.transform;
                Instantiate(escapeDoor, spawnPoint.transform.position, spawnPoint.transform.rotation);
            }
            else
            {
                Instantiate(Door, spawnPoint.transform.position, spawnPoint.transform.rotation);
            }

            yield return new WaitForSeconds(0.2f);
        }
    }
}
