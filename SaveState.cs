﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveState
{
    //PlayerData
    public int level = 1;
    public int stage = 1;
    public int highScore = 0;
    public int coins = 100;

    public int curLevel;

    //Settings
    public bool soundOn = true;
    public bool musicOn = true;
    public bool swipeOn = true;
    public bool touchOn = true;
}
