using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
	public float MinYaw = -360;
	public float MaxYaw = 360;
	public float MinPitch = -60;
	public float MaxPitch = 60;
	public float LookSensitivity = 1;

	public float MoveSpeed = 10;
	public float SprintSpeed = 30;
	private float currMoveSpeed = 0;

	CharacterController movementController;
	Camera playerCamera;

	public FixedTouchField touchField;

	public bool touchOn;

	bool isControlling;
	float yaw;
	float pitch;

	public float moveMagnitude;

	Vector3 velocity;


	public VariableJoystick variableJoystick;

	public Vector2 LookAxis;

	void Start() {

		movementController = GetComponent<CharacterController>();   //  Character Controller
		playerCamera = GetComponentInChildren<Camera>();            //  Player Camera
		isControlling = true;
	}

	void Update() {

		float horizontalMove = 0;
		float verticalMove = 0;

		Vector3 direction = Vector3.zero;

		if(touchOn == true)
		{
			horizontalMove = variableJoystick.Horizontal;
			verticalMove = variableJoystick.Vertical;

			moveMagnitude = Mathf.Abs(horizontalMove + verticalMove);
		}
		else if(touchOn == false)
		{
			horizontalMove = Input.GetAxisRaw("Horizontal");
			verticalMove = Input.GetAxisRaw("Vertical");

			moveMagnitude = Mathf.Abs(horizontalMove + verticalMove);
		}
		direction += transform.forward * verticalMove;
		direction += transform.right * horizontalMove;

		direction.Normalize();

		if (movementController.isGrounded) {
			velocity = Vector3.zero;
		} else {
			velocity += -transform.up * (9.81f * 10) * Time.deltaTime; // Gravity
		}

		if (Input.GetKey(KeyCode.LeftShift)) {  // Player can sprint by holding "Left Shit" keyboard button
			currMoveSpeed = SprintSpeed;
		} else {
			currMoveSpeed = MoveSpeed;
		}

		direction += velocity * Time.deltaTime;
		movementController.Move(direction * Time.deltaTime * currMoveSpeed);

		LookAxis = touchField.TouchDist;

		float mouseX = 0;
		float mouseY = 0;

		float LookSenst = 0;

		if(touchOn == true)
		{
			mouseX = LookAxis.x;
			mouseY = LookAxis.y;
			LookSenst = LookSensitivity;
		}

		else if(touchOn == false)
		{
			mouseX = Input.GetAxisRaw("Mouse X");
			mouseY = Input.GetAxisRaw("Mouse Y");

			LookSenst = LookSensitivity * 4.9f;
		} 

		// Camera Look
		yaw += mouseX * LookSenst;
		pitch -= mouseY * LookSenst;

		yaw = ClampAngle(yaw, MinYaw, MaxYaw);
		pitch = ClampAngle(pitch, MinPitch, MaxPitch);

		transform.eulerAngles = new Vector3(0.0f, yaw, 0.0f);
	}

	float ClampAngle(float angle) {
		return ClampAngle(angle, 0, 360);
	}

	float ClampAngle(float angle, float min, float max) {
		if (angle < -360)
			angle += 360;
		if (angle > 360)
			angle -= 360;

		return Mathf.Clamp(angle, min, max);
	}
	

}
