﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LoadUp : MonoBehaviour
{
    [SerializeField] Image img;

    void Start()
    {
        Invoke("OnLoadUp", 2f);
    }

    void OnLoadUp()
    {
        StartCoroutine(FadeImage());
    }

    // Update is called once per frame
    IEnumerator FadeImage()
    {
        // loop over 1 second backwards
        for (float i = 1; i >= 0; i -= (Time.deltaTime / 4f))
        {
            // set color with i as alpha
            img.color = new Color(img.color.r, img.color.g, img.color.b, i);
            yield return null;
        }
    }
}
