﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowScript : MonoBehaviour
{
	public Transform target;
	public Vector3 offset;

	
	void Start()
	{
		Invoke("on", 1f);
	}

	void on()
	{
		this.enabled = true;
	}
	// Update is called once per frame
	void Update () 
	{
		transform.position = target.position + offset;
		transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
	}
}