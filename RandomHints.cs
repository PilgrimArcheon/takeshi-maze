﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomHints : MonoBehaviour
{
    public Text text;
    public string[] hints;

    // Start is called before the first frame update
    void Start()
    {
        int rand = Random.Range(0, hints.Length);
        text.text = hints[rand];
    }
}
