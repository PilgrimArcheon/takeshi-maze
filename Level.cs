﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level : MonoBehaviour
{
    public int levelID;
    public bool open;
    public Button button;
    public Image levelImage;
    public Text text;

    public Color[] color;

    int curLevel;

    // Start is called before the first frame update
    void Start()
    {
        curLevel = SaveManager.Instance.state.level;
        

        if(levelID <= curLevel)
        {
            button.interactable = true;
            levelImage.color = color[0];
            text.color = color[0];
        }
        else
        {
            button.interactable = false;
            levelImage.color = color[2];
            text.color = color[2];
        }
    }

    public void Select()
	{
		open = true;
        levelImage.color = color[1];
        text.color = color[1];
		
	}

	public void Deselect()
	{
		open = false;
        levelImage.color = color[0];
        text.color = color[0];
	}

}
