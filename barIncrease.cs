﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class barIncrease : MonoBehaviour
{
    [SerializeField] Image bar;

    void OnEnable()
    {
        bar.fillAmount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(bar.fillAmount >= 1)
        {
            Invoke("Play", 2f);
             
        }

        else
        {
            bar.fillAmount += (Time.deltaTime * 0.1f);
        }
        
    }

    void Play()
    {
        MenuManager.Instance.OpenMenu("main");
    }
}
