﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameSceneLoader : MonoBehaviour
{
    [SerializeField] Image bar;

    void OnEnable()
    {
        bar.fillAmount = 0;
        StartCoroutine(LoadAysnchronously());
    }


    // Update is called once per frame
    IEnumerator LoadAysnchronously()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync("GameScene");        

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            LevelManager.Instance.PlayLevel();

            bar.fillAmount = progress;

            yield return null;
        }

    }
}
