﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    public GameObject[] Doors;
    public GameObject WallBlock;
    // Start is called before the first frame update

    int rand;

    bool spawned = false;

    void Start()
    {
        Invoke("Spawn", 0.1f);
        rand = Random.Range(0, Doors.Length);
    }

    void Spawn()
    {
        if(spawned == false)
        {
            Instantiate(WallBlock, transform.position, transform.rotation);
            spawned = true;
        }       
    }


    void OnTriggerEnter(Collider other) 
    {
        if(other.CompareTag("DoorSpace") && other.GetComponent<DoorTrigger>().spawned == true)
        {
            Destroy(gameObject);
            Instantiate(Doors[rand], transform.position, transform.rotation);
            
        }
    }

}
