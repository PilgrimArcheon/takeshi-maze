using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MonsterAI : MonoBehaviour
{
    public NavMeshAgent agent;

    public Transform player;

    public LayerMask whatIsGround, whatIsPlayer;

    GameObject grid;
    GameObject point;
    public Animator anim;


    //Patroling
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    bool alreadyAttacked;

    //States
    public float sightRange, captureRange;
    public bool playerInSightRange, playerInCaptureRange;

    private void Awake()
    {
        player = GameObject.Find("Player").transform;
        agent = GetComponent<NavMeshAgent>();

        grid = GameObject.Find("Grid");
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        //Check for sight and attack range
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        playerInCaptureRange = Physics.CheckSphere(transform.position, captureRange, whatIsPlayer);

        if (!playerInSightRange && !playerInCaptureRange) 
            Patroling(); 

        if (playerInSightRange && !playerInCaptureRange) 
            ChasePlayer();

        if (playerInCaptureRange && playerInSightRange) 
            CapturePlayer();
    }

    private void Patroling()
    {
        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet)
            agent.SetDestination(walkPoint);
            anim.SetBool("isChasing", false);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }

    void SearchWalkPoint()
    {
        int rand = Random.Range(0, LevelManager.Instance.noR);

        point = grid.transform.GetChild(rand).gameObject;

        //Calculate random point in range
        float pointZ = point.transform.position.z;
        float pointX = point.transform.position.x;

        walkPoint = new Vector3(pointX, transform.position.y, pointZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
            walkPointSet = true;
    }

    private void ChasePlayer()
    {
        agent.SetDestination(player.position);
        anim.SetBool("isChasing", true);
    }

    private void CapturePlayer()
    {
        //Make sure enemy doesn't move
        agent.SetDestination(transform.position);
        anim.SetBool("isChasing", false);

        transform.LookAt(player);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, captureRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }
}
